class Product:
    name = ""
    description = ""
    price = 0.00
    quantity = 0
    sizes = []
    gendor = ""
    vendor = ""
    product_type = ""
    photo_count = 0
    
    def __init__(self, name="", description="", price=0.00, quantity=0, sizes=[], gender="", vendor="", product_type="", photo_count=0):
        self.name = name
        self.description = description
        self.price = price
        self.quantity = quantity
        self.sizes = sizes
        self.gender = gender
        self.vendor = vendor
        self.product_type = product_type
        self.photo_count = photo_count

    def to_dict(self):
        return { 'name': self.name, 'description': self.description, 'price': round(self.price, 2), 'quantity': self.quantity, 'gender': self.gender, 'vendor': self.vendor, 'product_type': self.product_type, 'sizes': self.sizes, 'photo_count': self.photo_count }