# Required imports
import os
import requests
from flask import Flask, request, jsonify, render_template, make_response
from firebase_admin import credentials, firestore, initialize_app, storage
from .models import Product
from .query_catalog import query_catalog
from .send_email import send_email
import datetime

# Initialize Flask app
app = Flask(__name__)

# Initialize Firestore DB
cred = credentials.Certificate('key.json')
default_app = initialize_app(cred)
db = firestore.client()
inventory_ref = db.collection('inventory')

bucket_name = 'lucid-flair.appspot.com'

zip_code_tax_rates = {}

@app.route('/sitemap.xml')
def site_map():
    import datetime
    from urllib.parse import urlparse

    host_components = urlparse(request.host_url)
    host_base = host_components.scheme + "://" + host_components.netloc

    # Static routes with static content
    static_urls = list()
    for rule in app.url_map.iter_rules():
        if not str(rule).startswith("/admin") and not str(rule).startswith("/user"):
            if "GET" in rule.methods and len(rule.arguments) == 0:
                url = {
                    "loc": f"{host_base}{str(rule)}"
                }
                static_urls.append(url)

    # Dynamic routes with dynamic content
    inventory = query_catalog(inventory_ref, ['long-sleeved-shirt', 'short-sleeved-shirt', 'shoes', 'jewelry'])

    dynamic_urls = list()
    blog_posts = []#Post.objects(published=True)
    for item in inventory:
        url = {
            "loc": f"{host_base}/product/{item['name']} - {item['vendor']}"
            }
        dynamic_urls.append(url)

    xml_sitemap = render_template("sitemap.xml", static_urls=static_urls, dynamic_urls=dynamic_urls, host_base=host_base)
    response = make_response(xml_sitemap)
    response.headers["Content-Type"] = "application/xml"

    return response

@app.route('/lookupzipcode', methods=['POST'])
def look_up_zip_code():
    data = request.get_json()
    zip_code =  data['zip_code']
    if zip_code not in zip_code_tax_rates.keys():
        address = 'https://www.salestaxhandbook.com/api/' + zip_code
        response = str(requests.get(address).content)
        combined_rate = response.split('"CombinedRate":')[1].split(',"StateRate"')[0]
        zip_code_tax_rates[zip_code] = combined_rate
    else:
        combined_rate = zip_code_tax_rates[zip_code]
    return combined_rate, 200

@app.route('/order', methods=['POST'])
def order():
    data = request.get_json()
    product_id = data['product_id']
    id = str(data['create_time']) + product_id
    db.collection('orders').document(id).set(data)
    product_ref = inventory_ref.document(product_id)

    product_ref.update({"quantity": firestore.Increment(-1)})

    product_name = data['product_name']
    subject = product_name + ' just sold on Lucid Flair'
    payer_name = data['payer']['name']
    shipping_info = data['shipping_info']
    shipping_address = shipping_info['address']

    shipping_address_line_2 = ''
    if 'address_line_2' in shipping_address.keys():
        shipping_address_line_2 = shipping_address['address_line_2'] + ' '
    address = shipping_address['address_line_1'] + ' ' + shipping_address_line_2 + shipping_address['admin_area_2'] + ', ' + shipping_address['admin_area_1'] + '. ' + shipping_address['postal_code'] + '. ' + shipping_address['country_code']
    body = 'Please ship ' + product_name + ' to: ' + address + '. For: ' +  shipping_info['name']['full_name'] + '. ' + 'Purchased by: ' + payer_name['given_name'] + ' ' + payer_name['surname'] + '.'
    send_email('davebradham@gmail.com', subject, body)
    return 'order created'

@app.route('/new-inventory', methods=['GET'])
def inventory():
    return render_template('new_inventory.html')

@app.route('/new-inventory', methods=['POST', 'PUT'])
def new_inventory():
    try:
        from .upload_photo import upload_photo

        name = request.form['name']
        description = request.form['description']
        price = float(request.form['price'])
        quantity = int(request.form['quantity'])
        sizes = request.form.getlist('sizes')
        gender = request.form['gender']
        vendor = request.form['vendor']
        product_type = request.form['type']
                
        id = name + " - " + vendor
        
        photo_count = 0
        for i in range(1, 6):
            picture_string = 'picture' + str(i)
            file = request.files[picture_string]
            photo_count += upload_photo(id, file, i)

        model = Product(name, description, price, quantity, sizes, gender, vendor, product_type, photo_count)

        inventory_ref.document(id).set(model.to_dict())
        
        return render_template('home.html')
    except Exception as e:
        return f"An Error Occured: {e}"

@app.route('/shoes', methods=['GET', 'POST'])
def shoes():
    try:
        return catalog_helper(request, ['shoes'], 'Shoes')
    except Exception as e:
        return f"An Error Occured: {e}"

@app.route('/shirts', methods=['GET', 'POST'])
def shirts():
    try:
        shirts = ['long-sleeved-shirt', 'short-sleeved-shirt']
        return catalog_helper(request, shirts, 'Shirts')
    except Exception as e:
        return f"An Error Occured: {e}"

@app.route('/jewelry', methods=['GET', 'POST'])
def jewelry():
    try:
        return catalog_helper(request, ['jewelry'], 'Jewelry')     
    except Exception as e:
        return f"An Error Occured: {e}"

@app.route('/search', methods=['POST'])
def search():
    try:
        all_categories = ['long-sleeved-shirt', 'short-sleeved-shirt', 'shoes', 'jewelry']
        return catalog_helper(request, all_categories, "Results")
    except Exception as e:
        return f"An Error Occured: {e}"

@app.route('/list', methods=['GET'])
def read():
    try:
        all_categories = ['long-sleeved-shirt', 'short-sleeved-shirt', 'shoes', 'jewelry']
        return catalog_helper(all_categories, 'Everything')
    except Exception as e:
        return f"An Error Occured: {e}"

def catalog_helper(request, params=[], category=""):
    sort = request.form.get('sorting')
    sizes = request.form.getlist('sizes')
    vendors = request.form.getlist('vendors')
    keyword = ''
    if 'keyword' in request.form.keys():
        keyword = request.form['keyword']
    filters = { 'sizes': sizes, 'vendors': vendors, 'keyword': keyword }

    data = query_catalog(inventory_ref, params, filters)
    bucket = storage.bucket(app=default_app, name=bucket_name)
    for datum in data:
        blob = bucket.blob(datum['id'] + "/1.png")
        url = blob.generate_signed_url(datetime.timedelta(seconds=300), method='GET')
        datum['url'] = url
        datum['domId'] = datum['id'].replace(' ', '') + datum['size'].replace('.', '-')

    if sort == "price-high-to-low":
        data = sorted(data, key=lambda x: x['price'], reverse=True)
    elif sort == "price-low-to-high":
        data = sorted(data, key=lambda x: x['price'], reverse=False)

    return render_template('catalog.html', data = data, category = category, sort = sort, filters = filters)        

@app.route('/product/<string:id>')
def product(id):
    bucket = storage.bucket(app=default_app, name=bucket_name)
    pictures = []
    product = inventory_ref.document(id).get().to_dict()
    product['id'] = id
    for i in range(1, product['photo_count'] + 1):
        blob = bucket.blob(id + "/" + str(i) + ".png")
        five_minutes = datetime.timedelta(seconds=300)
        url = blob.generate_signed_url(five_minutes, method='GET')
        pictures.append(url)
    if len(product['sizes']) > 1:
        second_to_last_index = len(product['sizes']) - 1
        product['sizes'].insert(second_to_last_index, 'or')
    product['sizes'] = ', '.join(product['sizes'])
    return render_template('product.html', product = product, pictures = pictures)

@app.route('/thanks', methods=['GET'])
def thanks():
    name = request.args.get('name')
    return render_template('thanks.html', name = name)

@app.route('/home', methods=['GET'])
@app.route('/', methods=['GET'])
def home():
    return render_template('home.html')

port = int(os.environ.get('PORT', 8080))
if __name__ == '__main__':
    app.run(threaded=True, host='0.0.0.0', port=port)