from google.cloud import storage
from werkzeug.utils import secure_filename

# returns an int so we can count the number of uploaded photos per product
# keeping track of this in the db allows for better product pages/carousels
def upload_photo(id, file, pictureIndex):
    if file: 
        try:
            storage_client = storage.Client()
            buckets = list(storage_client.list_buckets())
            bucket = storage_client.bucket('lucid-flair.appspot.com')
            #file is just an object from request.files e.g. file = request.files['myFile']
            blob = bucket.blob(id + '/' + str(pictureIndex) + '.png')
            blob.upload_from_file(file)
            return 1
        except Exception as e:
            print('error uploading user photo: ', e)
    return 0
            