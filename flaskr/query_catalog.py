def should_filter(filters, item):
    if filters is None or filters == {}:
        return False
    if filters['vendors'] != [] and item['vendor'] not in filters['vendors']:
        return True
    if filters['sizes'] != [] and item['size'] not in filters['sizes']:
        return True
    if filters['keyword'] != '' and filters['keyword'].lower() not in item['name'].lower():
        return True
    return False


def query_catalog(inventory_ref, params=[], filters={}):
    if len(params) == 1:
        all_items = inventory_ref.where(u'product_type', u'==', params[0]).stream()
    else:
        all_items = inventory_ref.stream()
    result = []
    for item in all_items:
        item_dict = item.to_dict()
        if item_dict['quantity'] == 0:
            continue
        if item_dict['product_type'] in params:
            item_dict['id'] = item.id
            for size in item_dict['sizes']:
                item_dict['size'] = size
                if not should_filter(filters, item_dict):
                    result.append(item_dict.copy())
    return result