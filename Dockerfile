# Dockerfile
FROM python:3.7-stretch

RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential

ENV FLASK_APP=flaskr
ENV FLASK_RUN_PORT=8080

COPY . /app

WORKDIR /app

RUN pip install -r requirements.txt

CMD [ "flask", "run", "--host=0.0.0.0" ]